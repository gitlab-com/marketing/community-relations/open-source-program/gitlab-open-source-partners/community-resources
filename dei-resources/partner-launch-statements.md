# CHAOSS DEI Certification Integration
Open source partner launch statements

## Background
In preparation for the launch of [CHAOSS DEI certification integration with GitLab](https://about.gitlab.com/blog/2024/01/29/building-a-more-inclusive-and-welcoming-open-source-community-on-gitlab/?utm_campaign=devrel&utm_budget=devrel), we invited members of the GitLab Open Source Partners community to participate in the official announcement.
Partners supplied single-sentence quotations or lengthier statements in this file, and we worked with GitLab's editorial team to feature them in the launch announcement on the GitLab blog.

To craft statements, partners considered their responses to questions like:

- What excites you most about the launch of this integration?
- What impact do you hope this initiative will have on your project's DEI efforts?
- How will your community utilize this integration in its future DEI efforts?

## Statements

| Name | Role | Project |
| ---- | ---- | ------- |
| Emilio Salvador | VP, Developer Relations and Community | GitLab |
| Sherida McMullan | VP,  Diversity, Inclusion & Belonging | GitLab |
| Elizabeth Baron | Community Manager | CHAOSS Project |
| Tim Lehnen | Chief Technology Officer | Drupal Association |
| Alyssa Rock | Community manager | The Good Docs Project |
| Nils Brock | Program Director | Colmena | 
| Joe O'Gorman | Community manager | Kali Linux |

"GitLab is deeply committed to fostering a world where diversity, equity, and inclusion are at the forefront, empowering every individual to contribute and thrive. We look forward to collaborating with the CHAOSS Project and integrating our platform with the CHAOSS community's DEI (Diversity, Equity, and Inclusion) Badging initiative. We recognize that GitLab serves as vital infrastructure for numerous influential open source projects globally. By aiding these projects in attracting a diverse range of talent and encouraging the inclusion of varied voices and perspectives in their communities, we are not only enhancing the vibrancy and dynamism of these projects but also contributing to the overall health and sustainability of the open source software ecosystem. This initiative is a testament to our belief in the transformative power of diversity, equity, and inclusion. A heartfelt congratulations and immense gratitude go out to everyone involved in making this crucial collaboration a reality." — **Emilio Salvador, VP of Developer Relations and Community, GitLab**

"We are committed to diversifying open source communities on GitLab. It's a critical part of our strategy for Diversity, Equity, and Inclusion, at GitLab in 2024 and beyond. This DEI Project Badging program launched in partnership with CHAOSS helps us to make great strides in fostering an inclusive open source space and highlighting inclusive projects. As we enter Black History month, this is just the beginning of the impact we are looking to make in GitLab’s open source communities." — **Sherida McMullan, Vice President of Diversity, Inclusion & Belonging, GitLab**

"CHAOSS spends a lot of time thinking about open source community health, so we are thrilled to be able to help open source projects better communicate and surface their efforts to build more inclusive communities. We are hopeful that advocating for a more consistent way to do so (via a DEI.md file) will offer a better way for a project to share their approach with other projects, in true open source fashion. This is just a first step!" — **Elizabeth Baron, Community Manager, CHAOSS Project**

"The Drupal Association is proud to be reinforcing our longstanding commitment to diversity, equity, inclusion, and justice by partnering with CHAOSS and GitLab right at the launch of this initiative. Drupal is recognized as a Digital Public Good by the United Nations-endorsed Digital Public Goods Alliance, and we feel the responsibility of building a better, more open internet that recognizes, elevates, and serves historically underrepresented communities. We're hopeful that this effort is part of a sea change in open source communities, and software development in general, to better recognize, evaluate, and redress DEI challenges that we have a collective responsibility to solve. We believe this metric-driven approach will help projects reinforce each other's good behavior, and inspire the industry as a whole. We're looking forward to cataloging our DEI commitments according to this new process, to share and compare with the wider ecosystem." — **Tim Lehnen (hestenet), CTO, Drupal Association**

"The Good Docs Project is excited to join with CHAOSS and GitLab to promote the values of diversity, equity, and inclusion in open source. We want to empower our community members to do their best work and be their authentic selves. By participating in this initiative, we hope to think deeply about how we can promote greater diversity, equity, and inclusion in our project and then develop concrete policies and actions to support those goals. We pledge to develop our policies and earn our DEI badge from CHAOSS within the next few months." — **Alyssa Rock, Community manager, The Good Docs Project**

"The integration of CHAOSS project's diversity, equity, and inclusion (DEI) initiative with GitLab is an important milestone for building more inclusive open source software, one that resonates on all levels with our Colmena Project. The initiative creates the necessary visibility for many inclusive open source projects, not only paves the way for an ecosystem-focused approach to software development in general, but also encourages greater cooperation at a peer-to-peer level. It enables community members to recognize the vast diversity of contexts involved in the work of software development, and to inspire each other. This is important to the Colmena project, which is focused on supporting community and local media that makes visible the reality of indigenous peoples, women, youth, and different identities that are not part of the agenda of mainstream media. Participating in this initiative gives us the opportunity to better recognize DEI challenges and constantly reflect on our work to readjust and improve our efforts. We commit on continuing the dialogue with our community on these issues, documenting our efforts transparently and making necessary readjustments to policies and procedures." — **Nils Brock, Program Director, Colmena**

"The Kali Linux team is very proud to have been invited to take part in this initiative, and we are looking forward to what it means for the open source community. We are committed to being as inclusive as possible and hope to demonstrate that through our efforts. For more information on what we are planning on doing to support it, please read our [DEI Promise](https://www.kali.org/blog/dei-promise/)." — **Joe O'Gorman, Community Manager, Kali Linux**
